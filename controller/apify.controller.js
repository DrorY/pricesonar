const Apify = require('apify');
const { logger } = require('../helpers/winston');
const AWS = require('./aws.controller');
const MySQL = require('./mysql.controller');

/**
 * Declare global variables
 */
let screenBucketPath = null;
let dataBucketPathawait = null;
let browser = null;
let page = null;

/**
 * Main Apify function to scrape website
 * @param {URL to be scraped} scrapeURL
 */
async function ApifyScrapeAlert(scrapeURL) {
  let folderUrlName = null;
  folderUrlName = scrapeURL.match(/^(?:https?:)?(?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im)[1];
  const input = {
    url: scrapeURL,
    contentSelector: 'body',
    screenshotSelector: 'body',
  };
  if (!input.screenshotSelector) input.screenshotSelector = input.contentSelector;
  logger.info('Launching Puppeteer...');
  try {
    browser = await Apify.launchPuppeteer();
  } catch (error) {
    logger.error(`Error opening browser ${error}`);
  }
  logger.info(`Opening URL: ${input.url}`);
  try {
    page = await browser.newPage();
  } catch (error) {
    logger.error(`Error opening page with browser ${error}`);
  }
  const waitUntil = 'domcontentloaded';
  try {
    await page.setViewport({ width: 1920, height: 1080 });
    await page.goto(input.url, { waitUntil, timeout: 3600000 });
  } catch (error) {
    logger.error(`Error setting up page settings, ${error}`);
  }
  logger.info('Sleeping 5s ...');
  await new Promise(resolve => setTimeout(resolve, 5000));
  logger.info('Saving screenshot...');
  try {
    const screenshotBuffer = await page.screenshot({ fullPage: true });
    screenBucketPath = await AWS.saveToS3(screenshotBuffer, 'currentScreenshot.png', folderUrlName);
  } catch (e) {
    throw new Error('Cannot get screenshot (screenshot selector is probably wrong)');
  }
  logger.info('Saving data...');
  let content = null;
  try {
    content = await page.$eval(input.contentSelector, el => el.outerHTML);
    content = {
      website: scrapeURL,
      html: content,
    };
    content = JSON.stringify(content);
  } catch (e) {
    throw new Error('Cannot get content (content selector is probably wrong)');
  }
  dataBucketPathawait = await AWS.saveToS3(content, 'content.json', folderUrlName);
  logger.info('Closing Puppeteer...');
  await page.close();
  await browser.close();
  logger.info('Done with pupeteer,putting scrape into the db');
  const domain = scrapeURL.match(/^(?:https?:)?(?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im)[1];
  const data = {
    html: dataBucketPathawait.Location,
    screenshot_url: screenBucketPath.Location,
    website: domain,
  };
  try {
    await MySQL.insertScrape(data);
  } catch (error) {
    logger.error(`Error calling insertScrpe, ${error}`);
  }
}

module.exports = {
  ApifyScrapeAlert,
};
