const CronJob = require('cron').CronJob;
const { logger } = require('./winston');
const Apify = require('../controller/apify.controller');
const MySQL = require('../controller/mysql.controller');

/**
 * Cron runs every 01:00 AM every day
 */

const job = new CronJob('01 00 * * *', () => {
  MySQL.trackedWebsites().then(async (res) => {
    if (res.length !== 0) {
      for (let index = 0; index < res.length; index++) {
        logger.info(`Cron scrape for ${res[index]} website`);
        // eslint-disable-next-line no-await-in-loop
        await Apify.ApifyScrapeAlert(res[index]);
        logger.info('Cron finished');
      }
    }
  });
});
job.start();
