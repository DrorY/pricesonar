const db = require('../config/db.config.js');
const { logger } = require('../helpers/winston');

const Scrape = db.scrape;
const User = db.user;
const Website = db.website;
const Team = db.teamStatus;
const Transactions = db.transactions;

// insert scrape information into the database `scrapes` table
async function insertScrape(data) {
  try {
    await Scrape.create({
      html: data.html,
      screenshot_url: data.screenshot_url,
      website: data.website,
    });
    logger.info('Success inserting scrape information into MySQL');
  } catch (error) {
    logger.info(`Error insering scrape into MySQL ${error}`);
  }
}

// insert user information into the database `users` table
async function insertUser(data) {
  try {
    await User.findOrCreate({
      where: { id: data.id },
      defaults: {
        id: data.id,
        team_id: data.team_id,
        team_name: data.team_name,
        email: data.email,
        convo_state: data.convo_state,
      },
    });
    logger.info('Success inserting user information into MySQL');
  } catch (error) {
    logger.error(`Error insering user into MySQL ${error}`);
  }
}

async function insertwebsite(data) {
  try {
    await Website.findOrCreate({
      where: { domain: data.domain },
      defaults: {
        userId: data.userId,
        domain: data.url,
        pricing_page: data.pricing_page,
        changes: data.changes,
        status: data.status,
      },
    });
    logger.info('Success inserting website information to the database');
  } catch (error) {
    logger.error(`Error inserting website into the database, ${error}`);
  }
}

async function trackedWebsites(id) {
  if (!id) {
    try {
      const websitesArr = [];
      const websites = await Website.findAll();
      websites.forEach((item) => {
        websitesArr.push(item.dataValues.domain);
      });
      logger.info(`Success fetching tracked websites by cron ${websitesArr}`);
      return websitesArr;
    } catch (error) {
      logger.error(`Error fetching tracked websites, ${error}`);
    }
  }
  try {
    const websitesArr = [];
    const websites = await Website.findAll({
      where: { userId: id },
    });
    websites.forEach((item) => {
      websitesArr.push(item.dataValues.domain);
    });
    logger.info(`Success fetching tracked websites by user ${websitesArr}`);
    return websitesArr;
  } catch (error) {
    logger.error(`Error fetching tracked websites, ${error}`);
  }
  return false;
}

async function updateConvo(userId, convoState) {
  try {
    await User.update({ convo_state: convoState }, { where: { id: userId } });
    logger.info('Convo state has been updated');
  } catch (error) {
    logger.error(`Error updating convo state, ${error}`);
  }
}

async function getConvo(userId) {
  try {
    let convoState = await User.findAll({
      where: { id: userId },
    });
    convoState = convoState[0].dataValues.convo_state;
    logger.info(`Success fetching convo state ${convoState}`);
    return convoState;
  } catch (error) {
    logger.error(`Error fetching convo state, ${error}`);
  }
  return false;
}

async function getTeamStatus(teamId) {
  try {
    let teamStatus = await Team.findAll({
      where: { team_id: teamId },
    });
    teamStatus = teamStatus[0].dataValues.team_status;
    logger.info(`Success fetching team status ${teamStatus}`);
    return teamStatus;
  } catch (error) {
    logger.error(`Error fetching team status, ${error}`);
  }
  return false;
}

async function insertTeam(team) {
  try {
    await Team.findOrCreate({
      where: { team_id: team.id },
      defaults: {
        who_added_id: team.createdBy,
        team_id: team.id,
        team_name: team.name,
        team_status: 'free',
        last_transaction: 'free',
        last_transaction_status: 'free',
        who_paid_id: 'free',
        bot_channel_id: team.incoming_webhook.channel_id,
        bot_channel: team.incoming_webhook.channel,
      },
    });
    logger.info('Success inserting team to db');
  } catch (error) {
    logger.error(`Error inserting team into the database ${error}`);
  }
}

async function insertTransaction(data) {
  try {
    await Transactions.create({
      id: data.id,
      description: data.description,
      payment_method: data.payment_method,
      status: data.status,
      final_payment_date: data.final_payment_date,
      team_id: data.team_id,
      who_paid_id: data.who_paid_id,
    });
    logger.info('Inserted transaction into the database');
  } catch (error) {
    logger.error(`Error inserting transaction into the database ${error}`);
  }
}

async function updateTeam(data) {
  try {
    await Team.update({
      team_id: data.team_id,
      team_status: data.team_status,
      last_transaction: data.last_transaction,
      last_transaction_status: data.last_transaction_status,
      who_paid_id: data.who_paid_id,
    }, { where: { team_id: data.team_id } });
    logger.info('Team status has been updated');
  } catch (error) {
    logger.error(`Error updating team status, ${error}`);
  }
}

module.exports = {
  insertScrape,
  insertUser,
  insertwebsite,
  trackedWebsites,
  updateConvo,
  getConvo,
  getTeamStatus,
  insertTeam,
  insertTransaction,
  updateTeam,
};
