const winston = require('winston');

const logger = winston.createLogger({
  transports: [
    new (winston.transports.Console)({
      timestamp: true,
      colorize: true,
    }),
  ],
});

logger.stream = {
  write(message) {
    logger.info(message);
  },
};

module.exports = { logger };
