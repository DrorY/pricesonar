# .ENV global variables declaration

## apify access configuration
### NOTE: apify crawler works asynchronously only with [PAID MEMBERSHIP](https://www.apify.com/pricing)
- APIFY_USER_ID=
- APIFY_TOKEN=

## MySQL database access configuration
- DB_NAME=
- DB_USER=
- DB_USER_PASSWORD=
- DB_HOST=
- POOL_MAX=
- POOL_MAX=
- POOL_ACQUIRE=
- POOL_IDLE=

## AWS access configuration
- AWS_ACCESS_KEY=
- AWS_SECRET=
- AWS_REGION=
- AWS_S3_BUCKET_NAME=

## Slack bot access configuration
- SLACK_CLIENT_ID=
- SLACK_CLIENT_SECRET
- SLACK_VERIFICATION_TOKEN

## URL and server port setup
- URL
- PORT

## PayPal app access configuration
- PP_CLIENT_ID
- PP_CLIENT_SECRET