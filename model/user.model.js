module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define('user', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    team_id: {
      type: Sequelize.STRING,
    },
    team_name: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    convo_state: {
      type: Sequelize.STRING,
      defaultValue: 'start',
    },
  });
  User.sync();
  return User;
};
