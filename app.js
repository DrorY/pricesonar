require('dotenv').config({
  path: './config/.env',
});
require('./bot/slack');
require('./helpers/cron');
const i18n = require('i18n');
const path = require('path');
const paypal = require('paypal-rest-sdk');
const { logger } = require('./helpers/winston');
const ngrok = require('ngrok');

// Check for valid bot setup information
if (!process.env.PP_CLIENT_ID || !process.env.PP_CLIENT_SECRET) {
  logger.error('Error: Specify clientId clientSecret in environment');
  process.exit(1);
}

// Configure PayPal environment
paypal.configure({
  mode: 'sandbox', // sandbox or live
  client_id: process.env.PP_CLIENT_ID,
  client_secret: process.env.PP_CLIENT_SECRET,
});

i18n.configure({
  locales: ['us'],
  directory: path.resolve('locales'),
  defaultLocale: 'us',
  logErrorFn: ((msg) => {
    logger.info(`i18n error ${msg}`);
  }),
});

(async function() {
  const url = await ngrok.connect();
  logger.info('ngrok is available through: ' + url);
})();
