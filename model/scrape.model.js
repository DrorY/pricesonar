module.exports = (sequelize, Sequelize) => {
  const Scrape = sequelize.define('scrape', {
    html: {
      type: Sequelize.STRING,
    },
    screenshot_url: {
      type: Sequelize.STRING,
    },
    website: {
      type: Sequelize.STRING,
    },
  });
  Scrape.sync();
  return Scrape;
};
