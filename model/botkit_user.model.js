module.exports = (sequelize, Sequelize) => {
  const BotkitUser = sequelize.define('botkit_user', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    access_token: {
      type: Sequelize.STRING,
    },
    scopes: {
      type: Sequelize.STRING,
    },
    team_id: {
      type: Sequelize.STRING,
    },
    user: {
      type: Sequelize.STRING,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
  BotkitUser.sync();
  return BotkitUser;
};
