const AWS = require('aws-sdk');
const { logger } = require('../helpers/winston');

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET,
  region: process.env.AWS_REGION,
});

const folderDateName = new Date().toString();

const s3 = new AWS.S3();

async function saveToS3(file, filename, folderWebsiteName) {
  return new Promise((resolve, reject) => {
    logger.info('Saving screenshot and scrapes raw to Amazon S3');
    s3.upload({
      Bucket: process.env.AWS_S3_BUCKET_NAME,
      Key: `${folderWebsiteName}/${folderDateName}/${filename}`,
      Body: file,
    }, (err, res) => {
      if (err) {
        logger.error('Error saving screenshot and scrape to Amazon S3');
        reject(new Error(JSON.stringify(err)));
      } else {
        resolve(res);
        logger.info('Information has been saved to S3');
      }
    });
  });
}

module.exports = {
  saveToS3,
};
