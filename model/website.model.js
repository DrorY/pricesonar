module.exports = (sequelize, Sequelize) => {
  const Website = sequelize.define('website', {
    userId: {
      type: Sequelize.STRING,
    },
    domain: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    pricing_page: {
      type: Sequelize.STRING,
    },
    changes: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
      default: 'waiting_for_brief',
    },
  });
  Website.sync();
  return Website;
};
