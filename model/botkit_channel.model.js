module.exports = (sequelize, Sequelize) => {
  const BotkitChannel = sequelize.define('botkit_channel', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
  BotkitChannel.sync();
  return BotkitChannel;
};
