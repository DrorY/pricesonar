const Sequelize = require('sequelize');
const { logger } = require('../helpers/winston');

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_USER_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
      max: process.env.POOL_MAX,
      min: process.env.POOL_MIN,
      acquire: process.env.POOL_ACQUIRE,
      idle: process.env.POOL_IDLE,
    },
  },
);
sequelize
  .authenticate()
  .then(() => {
    logger.info('Connection has been established successfully.');
  })
  .catch((err) => {
    logger.error('Unable to connect to the database:', err);
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.scrape = require('../model/scrape.model.js')(sequelize, Sequelize);
db.user = require('../model/user.model.js')(sequelize, Sequelize);
db.website = require('../model/website.model.js')(sequelize, Sequelize);
db.teamStatus = require('../model/teamStatus.model.js')(sequelize, Sequelize);
db.transactions = require('../model/transactions.model.js')(sequelize, Sequelize);
db.botkit_team = require('../model/botkit_team.model.js')(sequelize, Sequelize);
db.botkit_user = require('../model/botkit_user.model.js')(sequelize, Sequelize);
db.botkit_channel = require('../model/botkit_channel.model.js')(sequelize, Sequelize);

db.website.belongsTo(db.user);

sequelize.sync();

module.exports = db;
