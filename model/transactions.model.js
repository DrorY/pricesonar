module.exports = (sequelize, Sequelize) => {
  const Transactions = sequelize.define('transactions', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    description: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    payment_method: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    final_payment_date: {
      type: Sequelize.STRING,
    },
    team_id: {
      type: Sequelize.STRING,
    },
    who_paid_id: {
      type: Sequelize.STRING,
    },
  });
  Transactions.sync();
  return Transactions;
};
