module.exports = (sequelize, Sequelize) => {
  const BotkitTeam = sequelize.define('botkit_team', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    createdBy: {
      type: Sequelize.STRING,
    },
    name: {
      type: Sequelize.STRING,
    },
    url: {
      type: Sequelize.STRING,
    },
    token: {
      type: Sequelize.STRING,
    },
    bot: {
      type: Sequelize.STRING,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
  BotkitTeam.sync();
  return BotkitTeam;
};
