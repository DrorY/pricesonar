const Botkit = require('botkit');
const i18n = require('i18n');
const paypal = require('paypal-rest-sdk');
const request = require('request');
const mySQLStorage = require('botkit-storage-mysql')({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_USER_PASSWORD,
  database: process.env.DB_NAME,
});
const uuidv1 = require('uuid/v1');
const apify = require('../controller/apify.controller');
const MySQL = require('../controller/mysql.controller');
const { logger } = require('../helpers/winston');
const validate = require('../helpers/validator');
const keyboards = require('../helpers/keyboards');
const slackPayPal = require('./slack.paypal');

const controller = Botkit.slackbot({
  host: process.env.URL,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  clientSigningSecret: process.env.SLACK_CLIENT_SIGNING_SECRET,
  scopes: ['bot'],
  debug: true,
  pooling: true,
  storage: mySQLStorage,
});

/**
 * Set up global variables
 */

let userId = null;
let teamId = null;
let url = null;

/* Setup webserver, create endpoints for paypal */
controller.setupWebserver(process.env.PORT, (err, webserver) => {
  if (!err) {
    webserver.get('/', (req, res) => {
      res.sendStatus(200);
    });
    webserver.get('/createplan', async (req, res) => {
      res.sendStatus(200);
    });
    webserver.get('/cancel', async (req, res) => {
      let cancelPayment = null;
      try {
        cancelPayment = await slackPayPal.paymentCancel(req, res);
        logger.info(`Success calling cancel route, ${cancelPayment}`);
      } catch (error) {
        logger.error(`Error calling cancel route, ${error}`);
      }
    });
    webserver.get('/processagreement', async (req, res) => {
      let processPayment = null;
      try {
        processPayment = await slackPayPal.processAgreement(req, res);
        const data = {
          id: processPayment.id,
          description: processPayment.description,
          payment_method: processPayment.payer.payment_method,
          status: processPayment.payer.status,
          final_payment_date: processPayment.agreement_details.final_payment_date,
          team_id: teamId,
          who_paid_id: userId,
        };
        try {
          await MySQL.insertTransaction(data);
        } catch (error) {
          logger.error(`Error calling insert transaction, ${error}`);
        }
        const update = {
          team_id: teamId,
          team_status: 'silver',
          last_transaction: processPayment.agreement_details.final_payment_date,
          last_transaction_status: processPayment.payer.status,
          who_paid_id: userId,
        };
        try {
          await MySQL.updateTeam(update);
        } catch (error) {
          logger.error(`Error calling update team, ${error}`);
        }
      } catch (error) {
        logger.error(`Error calling process agreement, ${error}`);
      }
    });
    controller.createWebhookEndpoints(controller.webserver);
    controller.createOauthEndpoints(controller.webserver);
  } else {
    logger.info(`error setting up webserver ${err}`);
  }
});

/**
 * Listen for the adding bot the the team and insert team information into the team_status database
 * Billing fields are set to 'free'
 */

controller.on('create_team', async (bot, message) => {
  try {
    await MySQL.insertTeam(message);
  } catch (error) {
    logger.error(`Error inserting team, ${error}`);
  }
});


/**
 * Once user provides the url for the checkup, insert user and team information into database
 * @convo_state variable holds a state of the conversation for upcoming bot upgrade
 */
controller.on('user_channel_join', async (bot, message) => {
  try {
    await bot.api.users.info({ user: message.user }, async (error, res) => {
      try {
        await bot.api.team.info({ user: message.user }, async (err, response) => {
          if (error || err) {
            logger.error(`Could not get new user or team information to insert into the database ${error}, ${err}`);
          } else {
            userId = res.user.id;
            teamId = response.team.id;
            const data = {
              id: res.user.id,
              team_id: response.team.id,
              team_name: response.team.name,
              email: res.user.profile.email,
              convo_state: 'start',
            };
            try {
              await MySQL.insertUser(data);
            } catch (er) {
              logger.error(`Error inserting user to db ${er}`);
            }
          }
        });
      } catch (err) {
        logger.error(`Error getting team info, ${err}`);
      }
    });
  } catch (error) {
    logger.error(`Error getting user info, botkit ${error}`);
  }
  bot.reply(message, i18n.__('greetingText'));
});

/**
 * Listener for slash commands
 */
controller.on('slash_command', async (bot, message) => {
  if ((message.text === 'tracking' && message.command === '/show_tracking_websites')
    || (message.text === 'who' && message.command === '/show_tracking_websites')
    || (message.command === '/show_tracking_websites')) {
    logger.info('Displaying the list of tracking websites');
    let websites = null;
    try {
      websites = await MySQL.trackedWebsites(userId);
    } catch (error) {
      logger.error(`Error getting tracked websites, ${error}`);
    }
    bot.replyAcknowledge(() => {
      bot.reply(message, `${i18n.__('tracked_websites')}\n${websites.join('\n')}`);
    });
  }

  if (message.command === '/show_website_changes') {
    url = validate.returnSubdomain(message.text);
    if (url === false) {
      bot.reply(message, i18n.__('websiteEnteredIncorrect'));
    } else {
      const trackedWebsite = url.url;
      let websites = null;
      let status = null;
      try {
        websites = await MySQL.trackedWebsites(userId);
        try {
          await bot.api.team.info({ user: message.user }, async (err, response) => {
            if (err) {
              logger.error(`Could not get user's team ${err}`);
            } else {
              status = await MySQL.getTeamStatus(response.team.id);
              logger.info('Check if user sent domain to track and if present in DB');
              if (message.text.length === 0 || (websites.includes(trackedWebsite) === false)) {
                bot.reply(message, i18n.__('websiteIsNotInDBorEmptyLine'));
              } else {
                logger.info('Website in DB, checking for the team plan');
                logger.info(`Team status ${status}`);
                switch (status) {
                  case 'free': {
                    bot.replyAcknowledge(() => {
                      bot.reply(message, keyboards.purchaseSubscription());
                    });
                    break;
                  }
                  case 'silver': {
                    bot.reply(message, i18n.__('changesWIllShow'));
                    break;
                  }
                  default: {
                    break;
                  }
                }
              }
            }
          });
        } catch (error) {
          logger.error(`Error getting statuses, ${error}`);
        }
      } catch (error) {
        logger.error(`Error getting tracked websites, ${error}`);
      }
    }
  }
});

/**
 * Bot listens for website here
 */
controller.hears(['.*', 'interactive'], ['direct_message', 'direct_mention', 'mention', 'ambient'], async (bot, message) => {
  if (message.text.length !== 0 && message.text.match(/[<>]/g)) {
    url = message.text.replace(/[<>]/g, '');
    url = validate.returnSubdomain(url);
    const data = {
      userId,
      url: url.url,
      pricing_page: url.subdomain,
      changes: 'test',
      status: 'waiting_for_brief',
    };
    let websites = null;
    try {
      websites = await MySQL.trackedWebsites(userId);
    } catch (error) {
      logger.error(`Error getting tracked websites, ${error}`);
    }
    if (websites.includes(url.url)) {
      bot.reply(message, i18n.__('websiteAlreadyExists'));
    } else {
      let isPricingPage = null;
      let notPricingPage = null;
      try {
        isPricingPage = await validate.testSubdomain(url.url);
        if (isPricingPage.statusCode === 200) {
          logger.info(`/pricing page found with status ${isPricingPage.statusCode}`);
          bot.reply(message, keyboards.toTrackSuggested(`${url.url}/pricing`, url.url));
          try {
            await MySQL.updateConvo(userId, 'toTrackSuggestedPricingPage');
          } catch (error) {
            logger.error(`Error updaring convoState ${error}`);
          }
        } else {
          logger.info('Pricing page not found, checking for the provided page');
          notPricingPage = await validate.testDomain(url.url);
          if (notPricingPage.statusCode === 200) {
            logger.info(`Provided page is on the web, ${notPricingPage.statusCode}`);
            bot.reply(message, i18n.__('websiteEnteredCorrect'));
            try {
              await MySQL.insertwebsite(data);
            } catch (error) {
              logger.error(`Error inserting website into database ${error}`);
            }
            try {
              await apify.ApifyScrapeAlert(url.url, userId);
            } catch (error) {
              logger.error(`Error launching Apify ${error}`);
            }
          } else {
            bot.reply(message, i18n.__('websiteEnteredIncorrect'));
          }
        }
      } catch (error) {
        logger.error(`Error validating domain, ${error}`);
      }
    }
  } else {
    bot.reply(message, i18n.__('websiteEnteredIncorrect'));
  }
});

controller.on('interactive_message_callback', async (bot, message) => {
  const data = {
    userId,
    url: url.url,
    pricing_page: url.subdomain,
    changes: 'test',
    status: 'waiting_for_brief',
  };
  switch (message.actions[0].value) {
    case 'suggested': {
      bot.reply(message, i18n.__('websiteEnteredCorrect'));
      try {
        await MySQL.updateConvo(userId, 'start');
      } catch (error) {
        logger.error(`Error updaring convoState ${error}`);
      }
      try {
        await MySQL.insertwebsite(data);
      } catch (error) {
        logger.error(`Error inserting website into database ${error}`);
      }
      try {
        await apify.ApifyScrapeAlert(`${url.url}/pricing`, userId);
      } catch (error) {
        logger.error(`Error launching Apify ${error}`);
      }
      break;
    }
    case 'inserted': {
      bot.reply(message, i18n.__('websiteEnteredCorrect'));
      try {
        await MySQL.updateConvo(userId, 'start');
      } catch (error) {
        logger.error(`Error updaring convoState ${error}`);
      }
      try {
        await MySQL.insertwebsite(data, userId);
      } catch (error) {
        logger.error(`Error inserting website into database ${error}`);
      }
      try {
        await apify.ApifyScrapeAlert(url.url, userId);
      } catch (error) {
        logger.error(`Error launching Apify ${error}`);
      }
      break;
    }
    case 'cancel': {
      bot.reply(message, i18n.__('cancelSuggested'));
      try {
        await MySQL.updateConvo(userId, 'start');
      } catch (error) {
        logger.error(`Error updaring convoState ${error}`);
      }
      break;
    }
    case 'silver': {
      bot.reply(message, 'Generating your payment link. Hold on a moment...');
      const isoDate = new Date();
      const reply = {
        attachments: [{
          fallback: 'Payment initiation information failed to load.',
          color: '#5b6cff',
          pretext: 'Click the link below to initiate payment.',
          title: 'Purchase ',
          text: 'PayPal Payment Bot',
          thumb_url: 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2016-08-17/70252203425_a7e48673014756aad9a5_96.jpg',
          footer: 'PayPal',
          footer_icon: 'https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2016-08-17/70252203425_a7e48673014756aad9a5_96.jpg',
          ts: message.ts,
        }],
      };
      let billingPlanId = null;
      request(`${process.env.URL}/create`, async (err) => {
        if (!err) {
          try {
            billingPlanId = await slackPayPal.createBillingPlan();
            const billingAgreementAttributes = {
              name: i18n.__('membershipName'),
              description: i18n.__('membershipDescription'),
              start_date: isoDate,
              plan: {
                id: billingPlanId,
              },
              payer: {
                payment_method: 'paypal',
              },
            };
            isoDate.setSeconds(isoDate.getSeconds() + 4);
            // eslint-disable-next-line
            isoDate.toISOString().slice(0, 19) + 'Z';
            paypal.billingAgreement.create(
              billingAgreementAttributes,
              async (error, billingAgreement) => {
                if (error) {
                  logger.error(error);
                  throw error;
                } else {
                  const links = {};
                  billingAgreement.links.forEach((linkObj) => {
                    links[linkObj.rel] = {
                      href: linkObj.href,
                      method: linkObj.method,
                    };
                  });
                  // eslint-disable-next-line
                  if (links.hasOwnProperty('approval_url')) {
                    // eslint-disable-next-line
                    reply.attachments[0].title_link = links['approval_url'].href;
                    bot.reply(message, reply);
                  } else {
                    logger.error('no redirect URI present');
                  }
                }
              },
            );
          } catch (error) {
            logger.error(`Error createing billing plan, ${error}`);
          }
        }
      });
      break;
    }
    case 'pp_cancel': {
      bot.replyWithDialog(message, keyboards.dialogWindow);
      break;
    }
    default: {
      break;
    }
  }
});

/**
 * Recieve dialog form, validate response and handle errors and if closed
 */

controller.on('dialog_submission', async (bot, message) => {
  try {
    await bot.api.team.info({ user: message.user }, async (err, response) => {
      if (err) {
        logger.error(`Error getting team info ${err}`);
      } else {
        const transactionRefused = {
          id: uuidv1(),
          description: 'Refused by user',
          payment_method: 'Refused by user',
          status: JSON.stringify(message.submission),
          final_payment_date: 'Refused by user',
          team_id: response.team.id,
          who_paid_id: userId,
        };
        await MySQL.insertTransaction(transactionRefused);
      }
    });
    bot.reply(message, i18n.__('thxForFeedback'));
  } catch (error) {
    logger.error(`Error calling inserting refused transaction after submitting dialog, ${error}`);
  }
  bot.dialogOk();
});

controller.on('dialog_cancellation', async (bot, message) => {
  logger.info('Inserting refused transaction into db');
  try {
    await bot.api.team.info({ user: message.user }, async (err, response) => {
      if (err) {
        logger.error(`Error getting team info ${err}`);
      } else {
        const transactionRefused = {
          id: uuidv1(),
          description: 'Refused by user',
          payment_method: 'Refused by user',
          status: 'Feedback dialog cancelled',
          final_payment_date: 'Refused by user',
          team_id: response.team.id,
          who_paid_id: userId,
        };
        await MySQL.insertTransaction(transactionRefused);
      }
    });
    bot.reply(message, i18n.__('dialogWindowClosed'));
  } catch (error) {
    logger.error(`Error calling isert transaction, ${error}`);
  }
});

module.exports = {
  controller,
};
