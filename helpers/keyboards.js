/* eslint-disable comma-dangle */
const toTrackSuggested = (suggestedURL, instertedURL) => ({
  text: 'Hey, it seems you put main domain for your pricing page, but we found a pricing page on your site. Would you still like to track the page you entered?',
  attachments: [
    {
      text: 'Choose an option',
      fallback: 'There has been some problem, please try again',
      callback_id: 'track_suggested',
      color: '#3AA3E3',
      attachment_type: 'default',
      actions: [
        {
          name: 'url',
          text: suggestedURL,
          type: 'button',
          value: 'suggested'
        },
        {
          name: 'url',
          text: instertedURL,
          type: 'button',
          value: 'inserted'
        },
        {
          name: 'url',
          text: 'Cancel',
          style: 'danger',
          type: 'button',
          value: 'cancel'
        }
      ]
    }
  ]
});

const purchaseSubscription = () => ({
  text: 'In order to see the changes on the website please purchase the subscription.',
  attachments: [
    {
      text: 'Subscribtion is $19.99 monthly.',
      fallback: 'Something\'s gone wrong... try again.',
      callback_id: 'error_pp',
      color: '#3AA3E3',
      attachment_type: 'default',
      actions: [
        {
          name: 'paypal',
          text: '19.99 USD',
          type: 'button',
          value: 'silver'
        },
        {
          name: 'paypal',
          text: 'Cancel',
          style: 'danger',
          type: 'button',
          value: 'pp_cancel'
        }
      ]
    }
  ]
});

const closeWindow = '<h1 style="text-align: center">Thank you for your purchase, your payment has been accepted.</h1><h2>You may close the window</h2>';

const dialogWindow = {
  callback_id: 'feedback_dialog',
  title: 'Feedback',
  submit_label: 'Submit',
  notify_on_cancel: true,
  elements: [
    {
      label: 'Share your feedback',
      name: 'feedback',
      type: 'textarea',
      placeholder: 'Tell us why you had to cancel'
    },
    {
      label: 'Please enter your e-mail',
      name: 'email',
      type: 'text',
      subtype: 'email',
      placeholder: 'example@domain.com',
      optional: true
    }
  ]
};

module.exports = {
  toTrackSuggested,
  purchaseSubscription,
  closeWindow,
  dialogWindow,
};
