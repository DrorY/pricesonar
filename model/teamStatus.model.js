module.exports = (sequelize, Sequelize) => {
  const TeamStatus = sequelize.define('team_status', {
    who_added_id: {
      type: Sequelize.STRING,
    },
    team_id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    team_name: {
      type: Sequelize.STRING,
    },
    team_status: {
      type: Sequelize.STRING,
    },
    last_transaction: {
      type: Sequelize.STRING,
    },
    last_transaction_status: {
      type: Sequelize.STRING,
    },
    who_paid_id: {
      type: Sequelize.STRING,
    },
    bot_channel_id: {
      type: Sequelize.STRING,
    },
    bot_channel: {
      type: Sequelize.STRING,
    },
  });
  TeamStatus.sync();
  return TeamStatus;
};
