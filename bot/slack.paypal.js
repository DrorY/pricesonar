const paypal = require('paypal-rest-sdk');
const keyboards = require('../helpers/keyboards');
const { logger } = require('../helpers/winston');

async function createBillingPlan() {
  const billingPlanAttribs = {
    name: 'Membership: Standard',
    description: 'Monthly plan for my bot service',
    type: 'fixed',
    payment_definitions: [{
      name: 'Standard Plan',
      type: 'REGULAR',
      frequency_interval: '1',
      frequency: 'MONTH',
      cycles: '11',
      amount: {
        currency: 'USD',
        value: '19.99',
      },
    }],
    merchant_preferences: {
      setup_fee: {
        currency: 'USD',
        value: '1',
      },
      cancel_url: `${process.env.URL}/cancel`,
      return_url: `${process.env.URL}/processagreement`,
      max_fail_attempts: '0',
      auto_bill_amount: 'YES',
      initial_fail_amount_action: 'CONTINUE',
    },
  };
  const billingPlanUpdateAttributes = [{
    op: 'replace',
    path: '/',
    value: {
      state: 'ACTIVE',
    },
  }];
  return new Promise((resolve, reject) => {
    paypal.billingPlan.create(billingPlanAttribs, (error, response) => {
      if (error) {
        logger.error(`Error creating billing plan, ${error}`);
        reject(error);
      } else {
        logger.info(`Success creating billing plan, ${response.id}`);
        resolve(response);
      }
    });
  }).then(async (response) => {
    await new Promise((resolve, reject) => {
      paypal.billingPlan.update(response.id, billingPlanUpdateAttributes, (err, res) => {
        if (err) {
          logger.error(`Error updating the billing plan, ${err}`);
          reject(err);
        } else {
          logger.info('Success updating billing plan.');
          resolve(res);
        }
      });
    });
    return response.id;
  });
}

async function processAgreement(req, res) {
  const token = req.query.token;
  return new Promise((resolve, reject) => {
    paypal.billingAgreement.execute(token, {}, async (error, success) => {
      if (error) {
        logger.error(`Error on callback from paypal, ${error}`);
        reject(error);
      } else {
        logger.info('Successfully finished the billing agreement');
        res.send(await keyboards.closeWindow);
        resolve(success);
      }
    });
  });
}

async function paymentCancel(req, res) {
  const token = req.query.token;
  return new Promise((resolve, reject) => {
    paypal.billingAgreement.execute(token, {}, async (error, fail) => {
      if (error) {
        logger.error(`Error on callback from paypal, ${error}`);
        reject(error);
      } else {
        logger.info('Payment has failed, please try again.');
        res.send(await keyboards.closeWindow);
        resolve(fail);
      }
    });
  });
}

module.exports = {
  processAgreement,
  paymentCancel,
  createBillingPlan,
};
