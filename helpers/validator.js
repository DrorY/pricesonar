const request = require('request');
const { logger } = require('./winston');

const hasSubdomain = (url) => {
  const protocol = /^https?:\/\//;
  let noProtocol = null;

  if (url.match(protocol) && url !== null) {
    noProtocol = url.replace(protocol, '').split('/');
    if (noProtocol.length >= 2 && noProtocol[1].length !== 0) {
      return true;
    }
  }
  return false;
};

const returnSubdomain = (url) => {
  const protocolPattern = /^https?:\/\//im;
  const domainPattern = /^(?:https?:)?(?:\/\/)?(?:[^@\n]+@)?([^:\/\n]+)/im;
  const link = {};
  if (url.match(protocolPattern) && url !== null) {
    if (url[url.length - 1] === '/') {
      link.url = url.substr(0, url.length - 1);
    } else {
      link.url = url;
    }
    link.protocol = url.match(protocolPattern)[0];
    link.domain = url.match(domainPattern)[1];
    link.subdomain = url.replace(link.protocol + link.domain, '');
    return link;
  }
  return false;
};

const testSubdomain = async (url) => {
  const protocol = /^https?:\/\//;
  if (url.match(protocol) && url !== null) {
    return new Promise((resolve) => {
      request(`${url}/pricing`, (err, res) => {
        if (err) {
          logger.error(`Error reaching pricing page ${err}`);
        } else {
          logger.info('Reaching price page...');
          resolve(res);
        }
      });
    });
  }
  return `url passed is probably not valid ${url}`;
};

const testDomain = async (url) => {
  const protocol = /^https?:\/\//;
  if (url.match(protocol) && url !== null) {
    return new Promise((resolve) => {
      request(url, (err, res) => {
        if (err) {
          logger.error(`Error reaching pricing page ${err}`);
        } else {
          logger.info('Success reaching main page');
          resolve(res);
        }
      });
    });
  }
  return `url passed is probably not valid ${url}`;
};

module.exports = {
  hasSubdomain,
  returnSubdomain,
  testSubdomain,
  testDomain,
};
